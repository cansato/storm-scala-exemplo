name := "storm-scala"

version := "0.1"

scalaVersion := "2.13.1"

// https://mvnrepository.com/artifact/org.apache.storm/storm-core//
libraryDependencies += "org.apache.storm" % "storm-core" % "1.0.6"

assemblyJarName in assembly := name.value + "-assembly-" + scalaVersion.value.slice(0,4) + "-" + version.value + ".jar"

