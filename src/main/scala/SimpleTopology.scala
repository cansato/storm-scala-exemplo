import org.apache.storm.{Config, LocalCluster}
import org.apache.storm.topology.TopologyBuilder

object SimpleTopology {

  def main(args: Array[String]): Unit = {

    val builder = new TopologyBuilder

    builder.setSpout( "IntegerSpout", new IntegerSpout )

    builder.setBolt( "MultipleBolt", new MultipleBolt ).shuffleGrouping("IntegerSpout")

    val cluster = new LocalCluster

    cluster.submitTopology( "SimpleTopology", new Config, builder.createTopology() )

  }

}
