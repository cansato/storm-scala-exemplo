import java.util

import org.apache.storm.spout.SpoutOutputCollector
import org.apache.storm.task.TopologyContext
import org.apache.storm.topology.OutputFieldsDeclarer
import org.apache.storm.topology.base.BaseRichSpout
import org.apache.storm.tuple.{Fields, Values}

class IntegerSpout extends BaseRichSpout {

  var spoutOutputCollector: SpoutOutputCollector = null
  private var index: Integer = 0

  override def open(conf: util.Map[_, _], context: TopologyContext, collector: SpoutOutputCollector): Unit = {
    spoutOutputCollector = collector
  }

  override def nextTuple(): Unit = {
    if (index<100)
      spoutOutputCollector.emit(new Values(index));
    index+=1;
  }

  override def declareOutputFields(declarer: OutputFieldsDeclarer): Unit = {
    declarer.declare(new Fields("field"))
  }
}
